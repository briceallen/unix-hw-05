#include <unistd.h>
#include <sys/types.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <syscall.h>
#include <iostream>
#include <cstdarg>
#include <cstring>
#include <cstdlib>
#include <string>
#define FILEMODE (S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH)
#define MAXLINE 4096
static void	err_doit(int, int, const char *, va_list);

/*
 * Nonfatal error related to a system call.
 * Print a message and return.
 */
void
err_ret(const char *fmt, ...)
{
    va_list		ap;

    va_start(ap, fmt);
    err_doit(1, errno, fmt, ap);
    va_end(ap);
}

/*
 * Fatal error related to a system call.
 * Print a message and terminate.
 */
void
err_sys(const char *fmt, ...)
{
    va_list		ap;

    va_start(ap, fmt);
    err_doit(1, errno, fmt, ap);
    va_end(ap);
    exit(1);
}

/*
 * Nonfatal error unrelated to a system call.
 * Error code passed as explict parameter.
 * Print a message and return.
 */
void
err_cont(int error, const char *fmt, ...)
{
    va_list		ap;

    va_start(ap, fmt);
    err_doit(1, error, fmt, ap);
    va_end(ap);
}

/*
 * Fatal error unrelated to a system call.
 * Error code passed as explict parameter.
 * Print a message and terminate.
 */
void
err_exit(int error, const char *fmt, ...)
{
    va_list		ap;

    va_start(ap, fmt);
    err_doit(1, error, fmt, ap);
    va_end(ap);
    exit(1);
}

/*
 * Fatal error related to a system call.
 * Print a message, dump core, and terminate.
 */
void
err_dump(const char *fmt, ...)
{
    va_list		ap;

    va_start(ap, fmt);
    err_doit(1, errno, fmt, ap);
    va_end(ap);
    abort();		/* dump core and terminate */
    exit(1);		/* shouldn't get here */
}

/*
 * Nonfatal error unrelated to a system call.
 * Print a message and return.
 */
void
err_msg(const char *fmt, ...)
{
    va_list		ap;

    va_start(ap, fmt);
    err_doit(0, 0, fmt, ap);
    va_end(ap);
}

/*
 * Fatal error unrelated to a system call.
 * Print a message and terminate.
 */
void
err_quit(const char *fmt, ...)
{
    va_list		ap;

    va_start(ap, fmt);
    err_doit(0, 0, fmt, ap);
    va_end(ap);
    exit(1);
}

/*
 * Print a message and return to caller.
 * Caller specifies "errnoflag".
 */
static void
err_doit(int errnoflag, int error, const char *fmt, va_list ap)
{
    char	buf[MAXLINE];

    vsnprintf(buf, MAXLINE-1, fmt, ap);
    if (errnoflag)
        snprintf(buf+strlen(buf), MAXLINE-strlen(buf)-1, ": %s",
                 strerror(error));
    strcat(buf, "\n");
    fflush(stdout);		/* in case stdout and stderr are the same */
    fputs(buf, stderr);
        fflush(NULL);		/* flushes all stdio output streams */
}
inline std::string exitProgram(const std::string &pname) {
    std::cout << "Exiting program from " << pname << std::endl;
    pid_t tid;
    tid = (int) syscall(SYS_gettid);
    syscall(SYS_tgkill, getpid(), tid);
    exit(EXIT_SUCCESS);
}

int incNum(int fd, const std::string &stringname) {
    char buf[sizeof(long double)]; /// sixteen bytes
    if (lseek(fd, 0, SEEK_SET) == -1) {
        fprintf(stderr, "lseek failed\n");
        return EXIT_FAILURE;
    }
    if (read(fd, buf, sizeof(long double)) < 0) {
        fprintf(stderr, "failed to read file descriptor %d\n", fd);
        return EXIT_FAILURE;
    }

    sprintf(buf, "%d", atoi(buf) + 1);
    fprintf(stdout, "pid = %d, %s incremented file counter %s\n", getpid(), stringname.c_str(), buf);

    if (lseek(fd, 0, SEEK_SET) == -1) {
        fprintf(stderr, "lseek failed\n");
        return EXIT_FAILURE;
    }
    if (write(fd, buf, strlen(buf)) < 0) {
        fprintf(stderr, "write failed\n");
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

static volatile sig_atomic_t sigflag; /* set nonzero by sig handler */
static sigset_t newmask;
static sigset_t oldmask;
static sigset_t zeromask;

static void sig_usr([[maybe_unused]] int signo)    /* one signal handler for SIGUSR1 and SIGUSR2 */
{
    sigflag = 1;
}

void TELL_WAIT() {
    if (signal(SIGUSR1, sig_usr) == SIG_ERR)
        err_sys("signal(SIGUSR1) error");
    if (signal(SIGUSR2, sig_usr) == SIG_ERR)
        err_sys("signal(SIGUSR2) error");
    sigemptyset(&zeromask);
    sigemptyset(&newmask);
    sigaddset(&newmask, SIGUSR1);
    sigaddset(&newmask, SIGUSR2);

    /* Block SIGUSR1 and SIGUSR2, and save current signal mask */
    if (sigprocmask(SIG_BLOCK, &newmask, &oldmask) < 0)
        err_sys("SIG_BLOCK error");
}

void TELL_PARENT(pid_t pid) {
    kill(pid, SIGUSR2);        /* tell parent we're done */
}

void WAIT_PARENT() {
    while (sigflag == 0)
        sigsuspend(&zeromask);    /* and wait for parent */
    sigflag = 0;

    /* Reset signal mask to original value */
    if (sigprocmask(SIG_SETMASK, &oldmask, nullptr) < 0)
        err_sys("SIG_SETMASK error");
}

void TELL_CHILD(pid_t pid) {
    kill(pid, SIGUSR1);            /* tell child we're done */
}

void WAIT_CHILD() {
    while (sigflag == 0)
        sigsuspend(&zeromask);    /* and wait for child */
    sigflag = 0;

    /* Reset signal mask to original value */
    if (sigprocmask(SIG_SETMASK, &oldmask, nullptr) < 0)
        err_sys("SIG_SETMASK error");
}

int main([[maybe_unused]] int argc, [[maybe_unused]] char *argv[]) {
    try {
        const std::string parent = "parent";
        const std::string child = "child";
        int fd;
        unsigned short iter = 1000;
        if ((fd = open("output.txt", O_CREAT  | O_TRUNC | O_RDWR,FILEMODE )) < 0) { err_sys("open error"); }
        if (write(fd, "0", 1) != 1) { err_sys("write error"); }
        pid_t pid;
        TELL_WAIT();
        if ((pid = fork()) < 0) { err_sys("fork error"); }
        else if (pid == 0) {
            fprintf(stdout, "child pid = %d\n", getpid());
            while (iter--) {
                WAIT_PARENT();
                if (incNum(fd, child) < 0) err_sys("addition error");
                TELL_PARENT(getppid());
            }
            exitProgram(child);

        } else if (pid > 0) {
            fprintf(stdout, "parent pid = %d\n", getpid());
            while (iter--) {
                if (incNum(fd, parent) < 0) err_sys("addition error");
                TELL_CHILD(pid);
                WAIT_CHILD();
            }
        }
        if (waitpid(pid, nullptr, 0) < 0) err_sys("waitpid error");
        close(fd);
        exitProgram(parent);
    } catch (const std::exception &exc) {
        std::cerr << exc.what() << std::endl;
        perror("Exception caught exiting program;");
        _exit(EXIT_FAILURE);
    }
}


