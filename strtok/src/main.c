#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <stdlib.h>

#define errExit(msg)do { perror(msg); exit(EXIT_FAILURE); \
    } while (0)

static void sigintHandler(int sig) {
    static int i = 0;
    write(STDERR_FILENO, "Caught SIGINT!\n", 15);
    sleep(1);
    ++i;
    if(i==3){ _exit(EXIT_SUCCESS);}
}

char string[] = "a string, of, tokens, after strtok ";
char string1[] = "a string, of, tokens, after strtok_r ";

void strtok_(char *local_string) {
    char *token = NULL;
    char delim = ',';
    char const *cosnt_string = local_string;
    char *other_string;
    unsigned long length = strlen(cosnt_string) + 1;
    other_string = (char *) malloc(length);
    memcpy(other_string, cosnt_string, length);
    token = strtok(local_string, &delim);
    while (token != NULL) {
        printf("strtok token = %s\n", token);
        token = strtok(NULL, &delim);
    }
    printf("from strtok\n");
    for (int i = 0; i < length; i++)
        '\0' == other_string[i] ? printf("[null]") : printf("%c", other_string[i]);
    printf("\n\n");
    free(other_string);

}

int strtok_r_(const char *local_string) {
    char const *cosnt_string = local_string;
    char *other_string;
    unsigned long length = strlen(cosnt_string) + 1;
    other_string = (char *) malloc(length);
    memcpy(other_string, cosnt_string, length);
    char *token;
    char *save_pointer;
    token = strtok_r(other_string, " ,", &save_pointer);
    printf("r token: %s\n", token);
    while ((token = strtok_r(NULL, " ,", &save_pointer)))
        printf("r token: %s\n", token);
    for (int i = 0; i < length; i++)
        '\0' == other_string[i] ? printf("[null]") : printf("%c", other_string[i]);;
    printf("\n\n");
    free(other_string);
    return EXIT_SUCCESS; // for sure
}


int main(int argc, char *argv[]) {
	for(int i = 0; i < 5; ++i){
		printf("This program will not exit until 3 SIGINT have been caught.\n");
	}
    if (signal(SIGINT, sigintHandler) == SIG_ERR) errExit("signal SIGINT");
    signal(SIGINT, sigintHandler);
    while (1) {
        strtok_(string);
        sleep(2);
        strtok_r_(string1);
        sleep(2);
    }
}
