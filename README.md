# _Homework Five_

#### By _**Brice Allen**_

#### _Submission for homework five and extra credit_

## Technologies Used

* _UNIX_
* _C & C++_
* _VIM_
* _GitLab_

## Description

_Do exercise 10.6 on page 381. The source code of Figure 10.24 is
uploaded (tell wait.c) for your convenience in this homework. Even though it
would be somewhat different, you may also refer to the code in Figure 8.13 to
start with_
* a) Write incNum() function to read an integer number from a file, increment
the number by 1 and overwrite the number in the same file.
* b) Write your driver program as specified in the question 10.6 to test the syn-
chronization code. It’s okay to put the functions in tell wait.c and main
driver function in the same file.
* c) You may use lab2’s Makefile with some modifications of $ROOT and
$PROGS variables to compile this program.
* d) Hint: functions to consider using: open(), fork(), close(), lseek(), read(),
sprintf(), printf(), write()
You may use nanosleep() function (page 374) before calling incNum() to slowdown
the program execution speed. I put 10,000 nano seconds and that seemingly works
okay. Also, set a max counter variable to an integer (say 1,000) to automatically
terminate the program when it reaches the max counter number to handle the case
your program runs away. (It happened to me!) Also be careful NOT to put your code
in a while(true) { } loop to avoid a permanent busy-wait situation.

_10.6 page 381 APUE Write the following program to test the parent–child synchro-
nization functions in Figure 10.24. The process creates a file and writes the integer 0 to
the file. The process then calls fork, and the parent and child alternate incrementing the
counter in the file. Each time the counter is incremented, print which process (parent
or child) is doing the increment_

## Setup/Installation Requirements

* _cd into either directory_
* _make_
* _./hw5_
* _make clean_
* _done!_


_Each directory has its own makefile and will create a symlink to the executable named hw5. The extra credit portion has a signal handler that will not exit the program until SIGNINT has been caught 3 times._

## Known Bugs

* _No know issues_
* _compiles on Arch and the CSE grid_

## License

Copyright (c) _11/05/2022_ _Brice Allen_
